# VuePress Theme Portfolio

Demo: [frissdiegurke.com](https://frissdiegurke.com)

## Installation

```bash
npm install vuepress-theme-portfolio
```

## Usage

### Site configuration

Within your *.vuepress/config.js* file, add and customize:

```javascript
module.exports = {
  theme: "portfolio",
  themeConfig: {
    footer: "Copyright &copy; 2018-present Ole Reglitzki", // HTML-enabled footer
    nav: [ // same as default theme; see https://vuepress.vuejs.org/default-theme-config/#navbar
      {text: "Home", link: "/"},
    ],
    projectGroups: { // specify project groups
      applications: {
        title: "Applications",
        base: "/projects/applications",
      },
      libraries: {
        title: "Libraries",
        base: "/projects/libraries",
      },
    },
    labels: {
      "sample-label": {
        title: "Sample Label",
        classes: "light-green black-text", // see https://materializecss.com/color.html
      },
    }
  },
}
```

### Pages

Within pages you can control the behavior via the
[Front Matter](https://vuepress.vuejs.org/guide/markdown.html#front-matter) block.

Theme-specific Front Matter page data:

* `breadcrumb: String`, default: page title - The display value to use within the breadcrumb.
* `noContent: Boolean`, default: `false` - Hide the page content card (useful for pages with just project listings).
* `title: String`, default: first heading - As excerpts would display markdown-style headings, this should be preferred for project pages.
* `projectGroups: [String|{id:String, classes:String}]` - The project groups to display on this page. Useful classes are [column classes](https://materializecss.com/grid.html) like `s6`, etc.
* `project: Boolean`, default: `false` - Only project pages are listed within project groups.
* `labels: [String]` - A list of label keys to assign the project to.
* `sort: Number` - If specified, will be used for project order within project group listing.
* `details.stalled: String("YYYY.Q")` - If specified, will be used for sort order if no `sort` is provided. For later use within theme...
* `details.birth: String("YYYY.Q")` - If specified, will be used for sort order if no `sort` and `details.stalled` is provided. For later use within theme...
